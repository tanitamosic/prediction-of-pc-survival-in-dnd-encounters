import string
from sklearn.naive_bayes import CategoricalNB
from sklearn.model_selection import train_test_split
from pandas import read_csv
from numpy import column_stack, mean
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.feature_extraction.text import CountVectorizer

def load_data():
    data = read_csv('data.csv', sep=',')
    stats = column_stack((data.level.values,data.race.values,data.cclass.values,data.party_size.values,data.enemy.values)).tolist()
    outcomes = data.survived.values.tolist()
    return stats, outcomes

def count_words(text):
    words_count = {}
    for words in text:
        for word in words:
            if word in words_count:
                words_count[word] += 1
            else:
                words_count[word] = 1
    return words_count

def preprocess():
    stats,outcomes = load_data()

    for stat_line in stats:
        for stat in stat_line:
            if isinstance(stat, str):
                stat = stat.lower()
            else: 
                stat = str(stat)
    counts = count_words(stats)

    for outcome in outcomes:
        outcome =  1 if outcome == 'Survived' else 0
    one_line_stats = []
    for stat_line in stats:
        for stat in stat_line:
            one_line_stats.append(stat_line)
        stat_line = "".join("{0} ".format(stat) for stat in stat_line)
    
    one_line_stats = "".join("{0} ".format(stat) for stat in one_line_stats)

    counts = CountVectorizer().fit_transform([stats])
    transformer = TfidfTransformer().fit(counts)

    counts = transformer.transform(counts)
    
    return stats, outcomes, counts


if __name__ == "__main__":
    stats, outcomes, counts = preprocess()
    x_train, x_test, y_train, y_test = train_test_split(counts, outcomes, test_size=0.1)
    model =  CategoricalNB().fit(x_train, y_train)

    predicted = model.predict(x_test)
    print(mean(predicted == y_test))
