from __future__ import print_function

import re
import string
import math
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split

def load_data():
    data = pd.read_csv('data.csv', sep=',')
    stats = np.column_stack((data.level.values,data.race.values,data.cclass.values,data.party_size.values,data.enemy.values)).tolist()
    outcomes = data.survived.values
    return stats, outcomes


def preprocess(text):
    if isinstance(text,list):
        text = ''.join( word for word in text)
    text = text.replace("," , " ")
    exclude = set(string.punctuation)
    return ''.join(ch for ch in text if ch not in exclude)


def tokenize(text):
    text = preprocess(text)
    words = re.split("\W+", text)
    return words


def count_words(text):
    if isinstance(text, list):
        words = text
    else:
        words = tokenize(text)
    words_count = {}
    for word in words:
        if word in words_count:
            words_count[word] += 1
        else:
            words_count[word] = 1
    return words_count


def fit(stats, outcomes):
    # inicijalizacija struktura
    bag_of_words = {}              
    words_count = {'Survived': {},       
                   'Died': {}}
    texts_count = {'Survived': 0.0,      
                   'Died': 0.0}

    for stat, outcome in zip(stats, outcomes):
        text_dict = count_words(stat)

        for word, count in text_dict.items():
            if word in bag_of_words:
                bag_of_words[word] += count
            else:
                bag_of_words[word] = count

            if word in words_count[outcome]:
                words_count[outcome][word] += count
            else:
                words_count[outcome][word] = count

        texts_count[outcome] += 1

    return bag_of_words, words_count, texts_count


def predict(text, bag_of_words, words_count, texts_count):
    words = tokenize(text)          

    score_true, score_false = 0.0, 0.0

    sum_all_words = float(sum(bag_of_words.values()))

    sum_words = {}
    for sentiment in words_count.keys():
        sum_words[sentiment] = float(sum(words_count[sentiment].values()))

    sum_all_sentiments = float(sum(texts_count.values()))

    p_sentiments = {}
    for sentiment in texts_count.keys():
        p_sentiments[sentiment] = texts_count[sentiment] / sum_all_sentiments

    sum_sentiments = {'Survived': 0.0, 'Died': 0.0}

    for word in words:
        if word in bag_of_words:
            word_probability = bag_of_words[word] / sum_all_words

        for sentiment in sum_sentiments.keys():
            if word in words_count[sentiment] and words_count[sentiment][word] > 0:
                pp_word = words_count[sentiment][word] / sum_words[sentiment]
                sum_sentiments[sentiment] += math.log(pp_word/word_probability)

    score_true = math.exp(sum_sentiments['Survived'] + math.log(p_sentiments['Survived']))
    score_false = math.exp(sum_sentiments['Died'] + math.log(p_sentiments['Died']))

    return {'Survived': score_true, 'Died': score_false}


if __name__ == '__main__':
    stats, outcomes = load_data()

    bag_of_words, words_count, texts_count = fit(stats, outcomes)

    text = "L7,Human,Artificer,6,Bone Devil"

    predictions = predict(text, bag_of_words, words_count, texts_count)
    
    # s = True if predictions['Survived'] > predictions['Died'] else False
    # print("they live") if s else print("they ded")

    print('-'*30)
    print('Character: {0}'.format(text))
    print('Probability of survival): {0}'.format(predictions['Survived']))
    print('Probability of death: {0}'.format(predictions['Died']))

    
