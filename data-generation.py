import pandas
from random import choice
from math import ceil, floor

levels = [x for x in range(1,21)]

races = {
    "Dwarf": 0.5,
    "Elf": 0,
    "Halfling": 0,
    "Human": 1,
    "Dragonborn": 2,
    "Gnome": 0,
    "Half-Elf": 0.5,
    "Half-Orc": 2,
    "Tiefling": 2
}

# class name: hit die
classes = {
    "Artificer": 8,
    "Bard": 8, 
    "Barbarian": 12, 
    "Cleric": 8, 
    "Druid": 6, 
    "Fighter": 10, 
    "Monk": 6, 
    "Paladin": 10, 
    "Ranger": 10, 
    "Rogue": 6, 
    "Sorcerer": 6, 
    "Warlock": 6,
    "Wizard": 6
}

party_size = [x for x in range (1,8)]

# monster name: challenge rating
monsters = {
    "Aboleth": 10, 
    "Balor": 19, 
    "Basilisk": 3, 
    "Beholder": 13, 
    "Black Dragon": 21, 
    "Black Pudding": 4, 
    "Blue Dragon": 23, 
    "Bone Devil": 9, 
    "Bulette": 5, 
    "Chain Devil": 8, 
    "Chimera": 6, 
    "Cloaker": 8, 
    "Djinni": 11, 
    "Efreeti": 11, 
    "Erinyes": 12, 
    "Fire Giant": 9, 
    "Frost Giant": 8, 
    "Gelatinous Cube": 2, 
    "Ghoul": 1, 
    "Glabrezu": 9, 
    "Gorgon": 5, 
    "Green Dragon": 22, 
    "Horned Devil": 11, 
    "Hydra": 8, 
    "Ice Devil": 14, 
    "Iron Golem": 16, 
    "Kraken": 23, 
    "Lamia": 4, 
    "Lich": 21, 
    "Mammoth": 6, 
    "Marilith": 16, 
    "Mimic": 2, 
    "Mind Flayer": 7,
    "Mummy": 3,
    "Mummy Lord": 15, 
    "Ogre": 2, 
    "Oni": 7, 
    "Owlbear": 3, 
    "Purple Worm": 15, 
    "Rakshasa": 13, 
    "Red Dragon": 24, 
    "Roc": 11, 
    "Spirit Naga": 8,
    "Stone Giant": 7, 
    "Stone Golem": 10, 
    "Tarrasque": 30, 
    "Vampire": 13, 
    "White Dragon": 20, 
    "Wraith": 5, 
    "Wyvern": 6
}

def generate(num):

    data = [] # data matrix, columns: level, race, class, party_size, enemy, survived   
    for i in range (0,num+1):
        level = choice(levels)
        race = choice(list(races))
        pc_class = choice(list(classes))
        ps = choice(party_size)
        enemy = choice(list(monsters))

        cr=monsters[enemy]
        con = classes[pc_class] + races[race] 

        survived = categorize(i, level, ps, cr, con)
        s = "Survived" if survived == True else "Died"
        entry = ["L{}".format(level), race, pc_class, ps, enemy, s]
        data.append(entry)

    export(data)

def categorize(i, level, ps, cr, con):
    if (i+1)%400 == 0: # 1.25% chance of outliers
        return choice([True, False])
    elif ps == 1: # single player survives much weaker enemy
        if cr+5 < level: 
            return True
        if cr+4 < level and floor(cr/2) < con*(0.85): 
            return True
    elif cr <= level and ps >= 4: # challenge rating definition
        return True
    elif cr+3 <= level and ps > 2: # weaker enemies don't endanger multiple pcs
        return True
    elif cr-level <= 3 and ps-4 >= 2: # large groups survive stronger enemies
        return True
    elif cr == level and ps < 4 and (cr/ps)>con: # abouteven odds of survival in smaller groups with high con
        return choice([True,False])
    return False

def export(data):
    # for a in data: print(a)
    dataframe = pandas.DataFrame(data)
    dataframe.columns = ["level", "race", "cclass", "party_size", "enemy", "survived"]
    dataframe.to_csv("data.csv")
        
if __name__ == "__main__":
    generate(15000)
