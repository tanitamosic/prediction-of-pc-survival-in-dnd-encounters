from pandas import read_csv
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.svm import SVC
from sklearn.metrics import accuracy_score

races = {
    "Dwarf": 1,
    "Elf": 2,
    "Halfling": 3,
    "Human": 4,
    "Dragonborn": 5,
    "Gnome": 6,
    "Half-Elf": 7,
    "Half-Orc": 8,
    "Tiefling": 9
}

classes = {
    "Artificer": 8,
    "Bard": 8, 
    "Barbarian": 12, 
    "Cleric": 8, 
    "Druid": 6, 
    "Fighter": 10, 
    "Monk": 6, 
    "Paladin": 10, 
    "Ranger": 10, 
    "Rogue": 6, 
    "Sorcerer": 6, 
    "Warlock": 6,
    "Wizard": 6
}

# monster name: challenge rating
monsters = {
    "Aboleth": 10, 
    "Balor": 19, 
    "Basilisk": 3, 
    "Beholder": 13, 
    "Black Dragon": 21, 
    "Black Pudding": 4, 
    "Blue Dragon": 23, 
    "Bone Devil": 9, 
    "Bulette": 5, 
    "Chain Devil": 8, 
    "Chimera": 6, 
    "Cloaker": 8, 
    "Djinni": 11, 
    "Efreeti": 11, 
    "Erinyes": 12, 
    "Fire Giant": 9, 
    "Frost Giant": 8, 
    "Gelatinous Cube": 2, 
    "Ghoul": 1, 
    "Glabrezu": 9, 
    "Gorgon": 5, 
    "Green Dragon": 22, 
    "Horned Devil": 11, 
    "Hydra": 8, 
    "Ice Devil": 14, 
    "Iron Golem": 16, 
    "Kraken": 23, 
    "Lamia": 4, 
    "Lich": 21, 
    "Mammoth": 6, 
    "Marilith": 16, 
    "Mimic": 2, 
    "Mind Flayer": 7,
    "Mummy": 3,
    "Mummy Lord": 15, 
    "Ogre": 2, 
    "Oni": 7, 
    "Owlbear": 3, 
    "Purple Worm": 15, 
    "Rakshasa": 13, 
    "Red Dragon": 24, 
    "Roc": 11, 
    "Spirit Naga": 8,
    "Stone Giant": 7, 
    "Stone Golem": 10, 
    "Tarrasque": 30, 
    "Vampire": 13, 
    "White Dragon": 20, 
    "Wraith": 5, 
    "Wyvern": 6
}

def load_data():
    data = read_csv('data.csv', sep=',')
    return data

def preprocess():
    data = load_data()
    
    for i in range(1,21):
        data['level'] = data['level'].replace("L{0}".format(i), i)
    
    for race in races:
        data['race'] = data['race'].replace(race, races[race])

    for cclass in classes:
        data['cclass'] = data['cclass'].replace(cclass, classes[cclass])

    for enemy in monsters:
        data['enemy'] = data['enemy'].replace(enemy, monsters[enemy])
    
    data['survived'] = data['survived'].replace('Survived',1)
    data['survived'] = data['survived'].replace('Died',0)

    stats =  data.iloc[1:,1:5]
    outcomes = data.iloc[1:,6]
    return stats, outcomes

    
if __name__ == "__main__":
    stats, outcomes = preprocess()
    x_train, x_test, y_train, y_test = train_test_split(stats, outcomes, test_size=0.1)

    scaled_train = StandardScaler().fit_transform(x_train)
    scaled_test = StandardScaler().fit_transform(x_test)

    classifier = SVC(kernel='linear')

    classifier.fit(x_train,y_train)
    predictions = classifier.predict(x_test)

    print(accuracy_score(y_test,predictions))
    
